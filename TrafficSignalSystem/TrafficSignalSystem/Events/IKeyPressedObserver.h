#pragma once
#include "../View/Keyboard.h"
using View::Keyboard;


namespace Events
{
	struct IKeyPressedObserver
	{
		virtual void HandleEvent(Keyboard key) = 0;
	};
}