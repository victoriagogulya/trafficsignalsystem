#include "KeyPressedEvent.h"

namespace Events
{
	void KeyPressedEvent::Notify(Keyboard key)
	{
		for (unsigned int i = 0; i < observers.size(); ++i)
			observers[i]->HandleEvent(key);
	}
}