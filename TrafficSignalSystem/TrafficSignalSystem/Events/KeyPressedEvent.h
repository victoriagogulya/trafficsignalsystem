#pragma once
#include "IKeyPressedObserver.h"
#include <vector>
#include <iostream>

using std::vector;

namespace Events
{
	class KeyPressedEvent
	{
	private:
		vector<IKeyPressedObserver*> observers;

		KeyPressedEvent(){}
		KeyPressedEvent(const KeyPressedEvent&){}
		KeyPressedEvent& operator=(KeyPressedEvent&){}
		~KeyPressedEvent(){}
	public:
		static KeyPressedEvent& Get()
		{
			static KeyPressedEvent modelChangedEvent;
			return modelChangedEvent;
		}
		void Attach(IKeyPressedObserver* observer) { observers.push_back(observer); }
		void Clear() { observers.clear(); }
		void Notify(Keyboard key);
	};
}