#include "DateAndTime.h"

namespace Log
{
	DateAndTime::DateAndTime()
	{
		timeData = new tm();
	}

	DateAndTime::~DateAndTime()
	{
		delete timeData;
	}

	void DateAndTime::CalculateTimeRaw()
	{
		time(&timeRaw);
		localtime_s(timeData, &timeRaw);
	}

	void DateAndTime::CalculateDate(string& result)
	{
		_itoa_s(timeData->tm_year + 1900, buffer, SIZE, 10);
		result += buffer;
		result += '-';

		_itoa_s(timeData->tm_mon + 1, buffer, SIZE, 10);
		result += buffer;
		result += '-';

		_itoa_s(timeData->tm_mday, buffer, SIZE, 10);
		result += buffer;
		result += ' ';
	}

	void DateAndTime::CalculateTime(string& result)
	{
		_itoa_s(timeData->tm_hour, buffer, SIZE, 10);
		if (timeData->tm_hour < 10)
			result += '0';
		result += buffer;
		result += ':';

		_itoa_s(timeData->tm_min, buffer, SIZE, 10);
		if (timeData->tm_min < 10)
			result += '0';
		result += buffer;
		result += ':';

		_itoa_s(timeData->tm_sec, buffer, SIZE, 10);
		if (timeData->tm_sec < 10)
			result += '0';
		result += buffer;
		result += ' ';
	}

	string DateAndTime::GetDateAndTime()
	{
		CalculateTimeRaw();
		string result = "";
		CalculateDate(result);
		CalculateTime(result);
		return result;
	}
}