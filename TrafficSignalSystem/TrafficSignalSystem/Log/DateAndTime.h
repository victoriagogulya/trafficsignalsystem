#pragma once
#include <ctime>
#include <iostream>
#include <string>
using std::string;


namespace Log
{
	class DateAndTime
	{
	private:
		time_t timeRaw;
		tm * timeData;
		static const int SIZE = 5;
		char buffer[SIZE];

		void CalculateTimeRaw();
		void CalculateDate(string& result);
		void CalculateTime(string& result);

	public:
		DateAndTime();
		~DateAndTime();

		string GetDateAndTime();
	};
}