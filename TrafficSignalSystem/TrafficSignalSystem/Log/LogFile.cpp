#include "LogFile.h"

namespace Log
{
	LogFile::LogFile() : fileName("Log.txt")
	{
		file.open(fileName, std::ios::app);
	}

	LogFile::~LogFile()
	{
		if (file.is_open())
			file.close();
	}

	void LogFile::WriteToFile(string str)
	{
		if (file.is_open())
		{
			file << str << std::endl;
		}
	}
}