#pragma once
#include <fstream>
#include <string>
using std::ofstream;
using std::string;

namespace Log
{
	class LogFile
	{
	private:
		ofstream file;
		char* fileName;

	public:
		LogFile();

		~LogFile();

		void WriteToFile(string str);
	};
}