#include "Logger.h"

namespace Log
{
	char* Logger::LevelToString(Level level)
	{
		char* str = NULL;
		switch (level)
		{
		case INFO:
			str = "INFO";
			break;
		case WARNING:
			str = "WARNING";
			break;
		case ERR:
			str = "ERROR";
			break;
		}
		return str;
	}

	void Logger::Log(Level level, string location, string message)
	{
			string result = dateAndTime.GetDateAndTime() +
				LevelToString(level) + " " +
				location + " - " +
				message;
			logFile.WriteToFile(result);
	}
}