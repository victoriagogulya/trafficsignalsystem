#pragma once
#include "DateAndTime.h"
#include "LogFile.h"
#include <string>

namespace Log
{
	class Logger
	{
	public:
		enum Level
		{
			INFO,
			WARNING,
			ERR
		};

	private:
		DateAndTime dateAndTime;
		LogFile logFile;

		Logger(){}
		Logger(const Logger&){}
		Logger& operator=(Logger&){}
		~Logger(){}

		char* LevelToString(Level level);

	public:
		static Logger& Get()
		{
			static Logger logger;
			return logger;
		}

		void Log(Level level, string location, string message);
	};
}