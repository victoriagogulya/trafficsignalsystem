#include "Presenter\IPresenter.h"
#include "Presenter\TrafficSignalPresenter.h"
#include "View\ConsoleView.h"
#include "Model\TrafficSignalController.h"
#include "Events\KeyPressedEvent.h"
#include "Log\Logger.h"
using Presenter::TrafficSignalPresenter;
using Presenter::IPresenter;
using View::ConsoleView;
using Model::TrafficSignalController;
using Events::KeyPressedEvent;
using Log::Logger;


void main()
{
	Logger::Get().Log(Logger::INFO, "Main.cpp:17", "Start");

	ConsoleView::Configure(true);

	const int timeRed = 2;
	const int timeYellow = 4;
	const int timeGreen = 3;

	IPresenter* presenter = new TrafficSignalPresenter
		(new ConsoleView(KeyPressedEvent::Get()),
		new TrafficSignalController(timeRed, timeYellow, timeGreen), KeyPressedEvent::Get());

	presenter->Run();
	delete presenter;

	ConsoleView::Configure(false);

	Logger::Get().Log(Logger::INFO, "Main.cpp:34", "End");
}