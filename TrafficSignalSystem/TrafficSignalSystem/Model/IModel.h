#pragma once
#include "TrafficSignal.h"
using Model::TrafficSignal;
#include <vector>

namespace Model
{
	struct IModel
	{
		virtual const std::vector<TrafficSignal*>& GetTrafficSignals() const = 0;
		virtual void Add(TrafficSignal* trafficSignal) = 0;
		virtual void Update() = 0;
		virtual bool IsChanged() const = 0;
		virtual ~IModel(){}
	};
}