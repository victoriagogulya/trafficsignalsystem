#pragma once

namespace Model
{
	enum LightsColor
	{
		RED,
		RED_YELLOW,
		GREEN,
		GREEN_YELLOW
	};
}