#pragma once

namespace Model
{
	struct Location
	{
	private:
		int x;
		int y;
		int width;
		int height;

	public:
		Location(int x = 0, int y = 0, int width = 1, int height = 1) : x(x), y(y), width(width), height(height){}

		void SetX(int x) { this->x = x < 0 ? 0 : x; }
		void SetY(int y) { this->y = y < 0 ? 0 : y; }
		void SetWidth(int width) { this->width < 0 ? 0 : width; }
		void SetHeight(int height) { this->height = height < 0 ? 0 : height; }

		int GetX() const { return x; }
		int GetY() const { return y; }
		int GetWidth() const { return width; }
		int GetHeight() const { return height; }
	};
}