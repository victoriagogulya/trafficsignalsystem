#pragma once


namespace Model
{
	class Timer
	{
	private:
		int delay;
		int millisecondsCounter;
		static const int INACCURACY_OF_MEASUREMENTS = 30;

	public:
		Timer(int delay) : delay(delay), millisecondsCounter(0){}

		void SetDelay(int delay) { this->delay = delay; }
		int GetDelay() const { return delay; }
		int GetTimeInMilliseconds() const { return millisecondsCounter; }

		void Update();
		void Reset() { millisecondsCounter = 0; }
	};
}