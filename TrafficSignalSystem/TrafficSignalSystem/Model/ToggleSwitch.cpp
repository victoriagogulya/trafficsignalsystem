#include "ToggleSwitch.h"

namespace Model
{
	ToggleSwitch::ToggleSwitch(int timeRed, int timeYellow, int timeGreen) : timer(500), position(0), state(ON)
	{
		timeIntervals[0] = timeRed;
		timeIntervals[1] = timeYellow;
		timeIntervals[2] = timeGreen;
		timeIntervals[3] = timeYellow;
	}

	void ToggleSwitch::ChangePosition()
	{ 
		position = position == 3 ? 0 : position + 1; 
	}

	void ToggleSwitch::Update()
	{
		state = OFF;
		timer.Update();
		if (timer.GetTimeInMilliseconds() / 1000 == timeIntervals[position])
		{
			ChangePosition();
			state = ON;
			timer.Reset();
		}
	}
}