#pragma once
#include "Timer.h"

namespace Model
{
	class ToggleSwitch
	{
	public:
		enum State { OFF, ON };

	private:
		Timer timer;
		static const int SIZE = 4;
		int timeIntervals[4];
		int position;
		State state;

		void ChangePosition();

	public:
		ToggleSwitch(int timeRed, int timeYellow, int timeGreen);

		State GetState() const { return state; }
		void Update();
	};
}