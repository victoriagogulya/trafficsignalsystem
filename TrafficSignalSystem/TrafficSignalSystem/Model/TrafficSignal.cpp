#include "TrafficSignal.h"
#include <iostream>


namespace Model
{
	TrafficSignal::~TrafficSignal()
	{
		if (location)
			delete location;
	}

	char* TrafficSignal::ColorToString()
	{
		char* str = NULL;
		switch (currentColor)
		{
		case RED:
			str = "RED";
			break;
		case GREEN_YELLOW:
		case RED_YELLOW:
			str = "YELLOW";
			break;
		case GREEN:
			str = "GREEN";
			break;
		}
		return str;
	}
}