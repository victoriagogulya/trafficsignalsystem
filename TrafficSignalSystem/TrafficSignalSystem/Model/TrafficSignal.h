#pragma once
#include "LightsColor.h"
#include "Location.h"

namespace Model
{
	class TrafficSignal
	{
	protected:
		const char* ID;
		Location* location;
		LightsColor currentColor;
		const int lightsCount;

	public:
		TrafficSignal(const char* ID, Location* location, int lightsCount, LightsColor lightsColor = RED)
			: ID(ID), location(location), currentColor(lightsColor), lightsCount(lightsCount) {}

		virtual  ~TrafficSignal();

		void SetColor(LightsColor color) { currentColor = color; }
		void SetLocation(Location* location) { this->location = location; }
		LightsColor GetColor() const { return currentColor; }
		int GetLightsCount() const { return lightsCount; }
		const Location* Getlocation() const { return location; }
		const char* GetID() const { return ID; }
		char* ColorToString();

		virtual void ChangeColor() = 0;
	};
}