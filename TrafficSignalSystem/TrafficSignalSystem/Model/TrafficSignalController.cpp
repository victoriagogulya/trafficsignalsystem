#include "TrafficSignalController.h"
#include "../Log/Logger.h"
using Log::Logger;

namespace Model
{
	TrafficSignalController::~TrafficSignalController()
	{
		if (!trafficSignals.empty())
		{
			for (unsigned int i = 0; i < trafficSignals.size(); i++)
				delete trafficSignals[i];
			trafficSignals.clear();
		}
	}

	void TrafficSignalController::Add(TrafficSignal* trafficSignal)
	{
		if (trafficSignal)
		{
			trafficSignals.push_back(trafficSignal);
			string str = "CREATED - ID:";
			str += trafficSignal->GetID();
			str += ", Color:";
			str += trafficSignal->ColorToString();
			Logger::Get().Log(Logger::INFO, "TrafficSignalController.cpp:27", str);
		}
	}

	void TrafficSignalController::Update()
	{
		toggleSwitch.Update();
		if (toggleSwitch.GetState() == ToggleSwitch::ON)
		{
			for (unsigned int i = 0; i < trafficSignals.size(); i++)
			{
				trafficSignals[i]->ChangeColor();
			}
		}
	}
}