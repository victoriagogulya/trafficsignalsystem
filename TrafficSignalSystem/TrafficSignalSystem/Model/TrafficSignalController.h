#pragma once
#include <vector>
#include "TrafficSignal.h"
#include "ToggleSwitch.h"
#include "IModel.h"

using std::vector;

namespace Model
{
	class TrafficSignalController : public IModel
	{
	private:
		ToggleSwitch toggleSwitch;
		vector<TrafficSignal*> trafficSignals;

	public:
		TrafficSignalController(int timeRed, int timeYellow, int timeGreen) : toggleSwitch(timeRed, timeYellow, timeGreen) {}
		~TrafficSignalController();

		virtual const vector<TrafficSignal*>& GetTrafficSignals() const { return trafficSignals; }
		virtual void Add(TrafficSignal* trafficSignal);
		virtual void Update();
		virtual bool IsChanged() const { return toggleSwitch.GetState() == ToggleSwitch::ON; }
	};
}