#include "TrafficSignalThreeLights.h"
#include "../Log/Logger.h"
#include <string>
using Log::Logger;
using std::string;

namespace Model
{
	void TrafficSignalThreeLights::ChangeColor()
	{
		currentColor = currentColor == GREEN_YELLOW ? RED : (LightsColor)(currentColor + 1);
		string str = "COLOR CHANGED - ID:";
		str += ID;
		str += ", Color:";
		str += ColorToString();
		Logger::Get().Log(Logger::INFO, "TrafficSignalThreeLights.cpp:10", str);
	}
}