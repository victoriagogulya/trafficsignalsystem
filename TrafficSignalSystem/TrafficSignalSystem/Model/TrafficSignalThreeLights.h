#pragma once
#include "TrafficSignal.h"

namespace Model
{
	class TrafficSignalThreeLights : public TrafficSignal
	{
	public:
		TrafficSignalThreeLights(const char* ID, Location* location, int lightsCount) 
			: TrafficSignal(ID, location, lightsCount) {}

		virtual void ChangeColor();
	};
}