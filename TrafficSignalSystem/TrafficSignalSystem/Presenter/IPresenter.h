#pragma once

namespace Presenter
{
	struct IPresenter
	{
		virtual void Run() = 0;
	};
}