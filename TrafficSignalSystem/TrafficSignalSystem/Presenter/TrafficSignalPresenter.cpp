#include "TrafficSignalPresenter.h"
#include "../Model/TrafficSignalThreeLights.h"
#include "../Model/LightsColor.h"
#include "../Log/Logger.h"
using Log::Logger;


namespace Presenter
{
	TrafficSignalPresenter::TrafficSignalPresenter(IView* view, IModel* model, KeyPressedEvent& keyPressedEvent)
		: view(view), model(model), key(View::KEY_S), contentForView(model->GetTrafficSignals())
	{
		keyPressedEvent.Attach(this);
	}

	TrafficSignalPresenter::~TrafficSignalPresenter()
	{
		if (view)
			delete view;
		if (model)
			delete model;
	}

	void TrafficSignalPresenter::Run()
	{
		model->Add(new Model::TrafficSignalThreeLights("1", new Model::Location(10, 0, 10, 7), 3));
		TrafficSignal* signal = new Model::TrafficSignalThreeLights("2", new Model::Location(57, 0, 10, 7), 3);
		signal->SetColor(Model::GREEN);
		model->Add(signal);
		view->Draw(contentForView);
		UpdateModelAndView();
	}

	void TrafficSignalPresenter::HandleEvent(Keyboard key)
	{
		string str = "";
		switch (key)
		{
		case View::KEY_E:
			this->key = View::KEY_E;
			str += "EXIT PRESSED";
			Logger::Get().Log(Logger::INFO, "TrafficSignalPresenter.cpp:41", str);
			break;
		case View::KEY_S:
			if (this->key == View::KEY_P)
			{
				this->key = View::KEY_S;
				contentForView.pause = false;
				str += "START PRESSED";
				Logger::Get().Log(Logger::INFO, "TrafficSignalPresenter.cpp:49", str);
			}
			break;
		case View::KEY_P:
			if (this->key == View::KEY_S)
			{
				this->key = View::KEY_P;
				contentForView.pause = true;
				str += "PAUSE PRESSED";
				Logger::Get().Log(Logger::INFO, "TrafficSignalPresenter.cpp:58", str);
			}
			break;
		}
	}

	void TrafficSignalPresenter::UpdateModelAndView()
	{
		while (key != View::KEY_E)
		{
			if (key != View::KEY_P)
			{
				model->Update();
				if (model->IsChanged())
					contentForView.redraw = true;
			}
			view->Update(contentForView);
			contentForView.redraw = false;
		}
	}
}