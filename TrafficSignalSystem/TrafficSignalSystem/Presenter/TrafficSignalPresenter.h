#pragma once
#include "../View/IView.h"
#include "../Model/IModel.h"
#include "../Events/IKeyPressedObserver.h"
#include "../Events/KeyPressedEvent.h"
#include "../View/Content.h"
#include "IPresenter.h"
using View::IView;
using Model::IModel;
using Events::IKeyPressedObserver;
using Events::KeyPressedEvent;
using View::Content;


namespace Presenter
{
	class TrafficSignalPresenter : public IPresenter, public IKeyPressedObserver
	{
	private:
		IView* view;
		IModel* model;
		Keyboard key;
		Content contentForView;

		void UpdateModelAndView();

	public:
		TrafficSignalPresenter(IView* view, IModel* model, KeyPressedEvent& keyPressedEvent);
		~TrafficSignalPresenter();

		virtual void Run();
		virtual void HandleEvent(Keyboard key);
	};
}