#include "ConsoleView.h"
#include <conio.h>
#include "Keyboard.h"


namespace View
{

	void ConsoleView::Configure(bool startOfProgramm)
	{
		if (startOfProgramm)
			ShowCursor(false);
		else
		{
			system("cls");
			ShowCursor(true);
		}
	}

	void ConsoleView::Draw(const Content& content) const
	{
		if (!content.trafficSignals.empty())
		{
			for (unsigned int i = 0; i < content.trafficSignals.size(); i++)
			{
				if (content.trafficSignals[i]->GetLightsCount() == THREE_LIGHTS)
					DrawThreeLightsTrafficSignal(content.trafficSignals[i]);
			}
		}
	}

	void ConsoleView::Redraw(const Content& content) const
	{
		if (!content.trafficSignals.empty())
		{
			for (unsigned int i = 0; i < content.trafficSignals.size(); i++)
			{
				if (content.trafficSignals[i]->GetLightsCount() == THREE_LIGHTS)
					RedrawThreeLightsTrafficSignal(content.trafficSignals[i]);
			}
		}
	}

	void ConsoleView::Update(const Content& content) const
	{
		if (_kbhit())
		{
			int x = _getch();
			switch (x)
			{
			case ENTER:
				keyPressedEvent.Get().Notify(ENTER);
				break;
			case KEY_P:
			case KEY_p:
			case KEY_P_RUS:
			case KEY_p_RUS:
				keyPressedEvent.Get().Notify(KEY_P);
				break;
			case KEY_S:
			case KEY_s:
			case KEY_S_RUS:
			case KEY_s_RUS:
				keyPressedEvent.Get().Notify(KEY_S);
				break;
			case ESC:
			case KEY_E:
			case KEY_e:
			case KEY_E_RUS:
			case KEY_e_RUS:
				keyPressedEvent.Get().Notify(KEY_E);
				break;
			}
		}
		if (content.pause)
			DrawPause(35, 15);
		else
		{
			HidePause(35, 15);
			if (content.redraw)
				Redraw(content);
			DrawBlinkYellow(content);
		}
	}

	void ConsoleView::DrawBlinkYellow(const Content& content) const
	{
		static long counter = 0;
		if (!content.trafficSignals.empty())
		{
			for (unsigned int i = 0; i < content.trafficSignals.size(); i++)
			{
				if (content.trafficSignals[i]->GetLightsCount() == THREE_LIGHTS
					&& (content.trafficSignals[i]->GetColor() == Model::GREEN_YELLOW
					|| content.trafficSignals[i]->GetColor() == Model::RED_YELLOW))
				{
					if (counter % 2 == 0)
					{
						DrawLight(content.trafficSignals[i]->Getlocation()->GetX() + 1,
							content.trafficSignals[i]->Getlocation()->GetY() + 2 
							+ content.trafficSignals[i]->Getlocation()->GetHeight(),
							content.trafficSignals[i]->Getlocation()->GetWidth(),
							content.trafficSignals[i]->Getlocation()->GetHeight(), Brown, true);
					}
					else
					{
						HideLight(content.trafficSignals[i]->Getlocation()->GetX() + 1,
							content.trafficSignals[i]->Getlocation()->GetY() + 2 
							+ content.trafficSignals[i]->Getlocation()->GetHeight(),
							content.trafficSignals[i]->Getlocation()->GetWidth(),
							content.trafficSignals[i]->Getlocation()->GetHeight());
						ConsoleColor colorYellow = (content.trafficSignals[i]->GetColor() == Model::GREEN_YELLOW
							|| content.trafficSignals[i]->GetColor() == Model::RED_YELLOW) ? Yellow : Brown;
						DrawLight(content.trafficSignals[i]->Getlocation()->GetX() + 1,
							content.trafficSignals[i]->Getlocation()->GetY() + 2
							+ content.trafficSignals[i]->Getlocation()->GetHeight(),
							content.trafficSignals[i]->Getlocation()->GetWidth(),
							content.trafficSignals[i]->Getlocation()->GetHeight(), colorYellow,
							colorYellow == Brown ? true : false);
					}
				}
			}
			counter++;
		}
	}

	void ConsoleView::DrawPause(int x, int y) const
	{
		static long counter = 0;
		if (counter % 2 == 0)
		{
			WriteStr(x, y, "PAUSE");
			ChangeTextAttr(x, y, LightRed, Black, 5);
		}
		else
			HidePause(x, y);
		Sleep(500);
		counter++;
	}

	void ConsoleView::HidePause(int x, int y) const
	{
		WriteStr(x, y, "     ");
		ChangeTextAttr(x, y, Black, Black, 5);
	}

	void ConsoleView::RedrawThreeLightsTrafficSignal(const TrafficSignal* trafficSignal) const
	{
		ConsoleColor colorRed = trafficSignal->GetColor() == Model::RED ? LightRed : Red;
		ConsoleColor colorYellow = (trafficSignal->GetColor() == Model::GREEN_YELLOW 
			|| trafficSignal->GetColor() == Model::RED_YELLOW) ? Yellow : Brown;
		ConsoleColor colorGreen = trafficSignal->GetColor() == Model::GREEN ? LightGreen : Green;

		//RED 
		DrawLight(trafficSignal->Getlocation()->GetX() + 1,
			trafficSignal->Getlocation()->GetY() + 1,
			trafficSignal->Getlocation()->GetWidth(),
			trafficSignal->Getlocation()->GetHeight(), colorRed,
			colorRed == Red ? true : false);

		//YELLOW
		DrawLight(trafficSignal->Getlocation()->GetX() + 1,
			trafficSignal->Getlocation()->GetY() + 2 + trafficSignal->Getlocation()->GetHeight(),
			trafficSignal->Getlocation()->GetWidth(),
			trafficSignal->Getlocation()->GetHeight(), colorYellow,
			colorYellow == Brown ? true : false);

		//GREEN
		DrawLight(trafficSignal->Getlocation()->GetX() + 1,
			trafficSignal->Getlocation()->GetY() + 3 + trafficSignal->Getlocation()->GetHeight() * 2,
			trafficSignal->Getlocation()->GetWidth(),
			trafficSignal->Getlocation()->GetHeight(), colorGreen,
			trafficSignal->GetColor() != Model::GREEN);
	}

	void ConsoleView::DrawThreeLightsTrafficSignal(const TrafficSignal* trafficSignal) const
	{	
		DrawField(trafficSignal->Getlocation(), DarkGray);
		RedrawThreeLightsTrafficSignal(trafficSignal);
	}

	void ConsoleView::DrawLight(int x, int y, int width, int height, ConsoleColor color, bool off) const
	{
		if (!off)
		{
			for (int i = 0; i < height; i++)
			{
				ChangeTextAttr(x, y + i, color, color, width);
			}
		}
		else
		{
			const unsigned char body = 178;
			for (int i = 0; i < height; i++)
			{
				WriteChars(x, y + i, body, width);
				ChangeTextAttr(x, y + i, ConsoleColor::Black, color, width);
			}
		}
	}

	void ConsoleView::HideLight(int x, int y, int width, int height) const
	{
		for (int i = 0; i < height; i++)
		{
			WriteChars(x, y + i, ' ', width);
			ChangeTextAttr(x, y + i, ConsoleColor::Black, ConsoleColor::Black, width);
		}
	}

	void ConsoleView::DrawField(const Location* location, ConsoleColor color) const
	{
		const unsigned char LeftTop = 201;
		const unsigned char Horz = 205;
		const unsigned char Vert = 186;
		const unsigned char RightTop = 187;
		const unsigned char LeftBottom = 200;
		const unsigned char RightBottom = 188;
		const unsigned char LeftCross = 204;
		const unsigned char RightCross = 185;


		SetColor(color, Black);

		WriteChar(location->GetX(), location->GetY(), LeftTop);
		for (int i = location->GetX() + 1; i <= location->GetX() + location->GetWidth(); i++)
		{
			WriteChar(i, location->GetY(), Horz);
		}
		WriteChar(location->GetX() + location->GetWidth() + 1, location->GetY(), RightTop);
		for (int i = location->GetY() + 1; i <= location->GetY() + location->GetHeight(); i++)
		{
			WriteChar(location->GetX(), i, Vert);
			WriteChar(location->GetX() + location->GetWidth() + 1, i, Vert);
		}
		WriteChar(location->GetX(), location->GetY() + location->GetHeight() + 1, LeftCross);
		for (int i = location->GetX() + 1; i <= location->GetX() + location->GetWidth(); i++)
		{
			WriteChar(i, location->GetY() + location->GetHeight() + 1, Horz);
		}
		WriteChar(location->GetX() + location->GetWidth() + 1, location->GetY() + location->GetHeight() + 1, RightCross);

		int y = location->GetY();
		y += location->GetHeight() + 1;

		for (int i = location->GetY() + 1; i <= y + location->GetHeight(); i++)
		{
			WriteChar(location->GetX(), i, Vert);
			WriteChar(location->GetX() + location->GetWidth() + 1, i, Vert);
		}
		WriteChar(location->GetX(), y + location->GetHeight() + 1, LeftCross);
		for (int i = location->GetX() + 1; i <= location->GetX() + location->GetWidth(); i++)
		{
			WriteChar(i, y + location->GetHeight() + 1, Horz);
		}
		WriteChar(location->GetX() + location->GetWidth() + 1, y + location->GetHeight() + 1, RightCross);

		y += location->GetHeight() + 1;
		for (int i = y + 1; i <= y + location->GetHeight(); i++)
		{
			WriteChar(location->GetX(), i, Vert);
			WriteChar(location->GetX() + location->GetWidth() + 1, i, Vert);
		}
		WriteChar(location->GetX(), y + location->GetHeight() + 1, LeftBottom);
		for (int i = location->GetX() + 1; i <= location->GetX() + location->GetWidth(); i++)
		{
			WriteChar(i, y + location->GetHeight() + 1, Horz);
		}
		WriteChar(location->GetX() + location->GetWidth() + 1, y + location->GetHeight() + 1, RightBottom);


		SetColor(LightGray, Black);
	}
}