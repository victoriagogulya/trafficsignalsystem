#pragma once
#include "IView.h"
#include "ConsoleLib.h"
#include "../Model/Location.h"
#include "../Events/KeyPressedEvent.h"
using Events::KeyPressedEvent;
using Model::Location;


namespace View
{
	class ConsoleView : public IView
	{
	private:
		static const int THREE_LIGHTS = 3;
		static const int TWO_LIGHTS = 2;
		KeyPressedEvent& keyPressedEvent;

		void Redraw(const Content& content) const;
		void DrawThreeLightsTrafficSignal(const TrafficSignal* trafficSignal) const;
		void RedrawThreeLightsTrafficSignal(const TrafficSignal* trafficSignal)const;
		void DrawBlinkYellow(const Content& content) const;
		void DrawField(const Location* location, ConsoleColor color) const;
		void DrawLight(int x, int y, int width, int height, ConsoleColor color, bool off) const;
		void HideLight(int x, int y, int width, int height) const;
		void DrawPause(int x, int y) const;
		void HidePause(int x, int y) const;

	public:
		ConsoleView(KeyPressedEvent& keyPressedEvent) : keyPressedEvent(keyPressedEvent) { }

		static void Configure(bool startOfProgramm);
		virtual void Draw(const Content& content) const;
		virtual void Update(const Content& content) const;
	};
}