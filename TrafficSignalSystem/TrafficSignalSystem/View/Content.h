#pragma once
#include <vector>
#include "../Model/TrafficSignal.h"

using std::vector;
using Model::TrafficSignal;

namespace View
{
	struct Content
	{
		const vector<TrafficSignal*>& trafficSignals;
		bool pause;
		bool redraw;

		Content(const vector<TrafficSignal*>& trafficSignals) : trafficSignals(trafficSignals), pause(false), redraw(false) {}
	};
}