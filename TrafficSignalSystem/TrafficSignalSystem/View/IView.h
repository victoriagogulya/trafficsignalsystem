#pragma once
#include "Content.h"

namespace View
{
	struct IView
	{
		virtual void Draw(const Content& content) const = 0;
		virtual void Update(const Content& content) const = 0;
		virtual ~IView(){}
	};
}