#pragma once

namespace View
{
	enum Keyboard
	{
		ENTER = 13,
		ESC = 27,
		KEY_S = 83,
		KEY_s = 115,
		KEY_P = 80,
		KEY_p = 112,
		KEY_E = 70,
		KEY_e = 101,
		KEY_P_RUS = 135,
		KEY_p_RUS = 167,
		KEY_S_RUS = 155,
		KEY_s_RUS = 235,
		KEY_E_RUS = 147,
		KEY_e_RUS = 227
	};
}